FROM alpine:3.9

LABEL maintainer="eyJhb <eyjhbb@gmail.com>"

# VERSIONS
ENV GITCHK_VERSION 0.8.0

# requirements
RUN \
    apk add --no-cache curl git python3 && \
    python3 -m ensurepip && \
    python3 -m pip install --upgrade pip && \
    python3 -m pip install requests pyyaml

# get git-chglog
RUN \
    wget https://github.com/git-chglog/git-chglog/releases/download/${GITCHK_VERSION}/git-chglog_linux_amd64 -O /usr/local/bin/git-chglog && \
    chmod +x /usr/local/bin/git-chglog

# copy our files
COPY src/ /

# copy script
COPY release.sh /usr/bin/

# make it executable
RUN \
    chmod +x /usr/bin/release.sh

WORKDIR /src
