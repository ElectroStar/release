# Container - Release
This container is simply used, to create new releases on the Gitlab page!
Simple as that.

Simply add something like the example `.gitlab-ci.yml` and create a new file `.gitlab-ci-release.yml`, and you should be good to go!
Do keep in mind, that it needs a API key for Gitlab, so that the container can make the new release page.
Specify it under `Settings -> CI/CD -> Variables` as e.g. `GITLAB_API_KEY`, and the below script should work

`.gitlab-ci.yml`
```
deploy:
  stage: deploy
  image: registry.gitlab.com/deviosec/containers/release:latest
  script:
    - release.sh
  only:
    - tags
  expect:
    - /^(?!master).+@/
```

`.gitlab-ci-release.yml`
```
name: My $CI_PROJECT_NAME is awesome release $CI_COMMIT_SHORT_SHA
description: Something cool!
tag_name: $CI_COMMIT_TAG
changelog: $CI_COMMIT_TAG
assets_dir: assets/
```
